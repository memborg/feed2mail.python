#!/opt/local/bin/python
# -*- coding: utf-8 -*-
import ConfigParser
import inspect, os

class ConfigLoader:
    config = None

    def __init__(self):
        self.configPath = self.GetAppDir()
        self.config = ConfigParser.ConfigParser()
        self.config.read(self.configPath + '/config.ini')

    def Get(self, section, key):
        return self.config.get(section, key);

    def GetAppDir(self):
        return os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
