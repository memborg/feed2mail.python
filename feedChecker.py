#!/opt/local/bin/python
# -*- coding: utf-8 -*-

import urllib
import xml.etree.ElementTree as ET
from bufferHandler import *
from datetime import datetime
import time
from StringIO import StringIO
import logging
import uuid
import unicodedata, re
import codecs
from configLoader import *
from urlparse import urlparse

class AppURLopener(urllib.FancyURLopener):
    version = "Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; DEVICE INFO) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Mobile Safari/537.36 Edge/12"

urllib._urlopener = AppURLopener()

class Checker():

    entries = []

    def __init__(self, feed):
        self.config = ConfigLoader()
        self.logpath = self.config.Get('Path', 'LogPath')
        self.feedId = feed['Id']
        feedType = feed['feedType']
        self.feedTitle = feed['title']
        self.feedUpdated = feed['updated']
        self.feedLink = feed['link']
        self.feedURI = urlparse(self.feedLink)
        self.feedDomain = '{uri.scheme}://{uri.netloc}'.format(uri=self.feedURI)

        if ':' not in self.feedUpdated:
            self.feedUpdated += ' 00:00:00'

        self.feedUpdated = datetime.strptime(self.feedUpdated, '%Y-%m-%d %H:%M:%S')

        logging.basicConfig(level=logging.DEBUG, filename=self.logpath + '/log.txt')

        try:
            self.entries = []

            xml = self.__ReadFeed__(feed['link'])

            if xml is not None:
                if feedType == 1:
                    self.__ParseRSS2__(xml)

                if feedType == 2:
                    self.__ParseAtom1__(xml)

                bh = BufferHandler()
                for row in self.entries:
                    bh.AddEntry(row)
        except:
            logging.exception(self.feedTitle + '('+self.feedLink+')')

    def __ReadFeed__(self, link):
        f = urllib.urlopen(link)
        xml = f.read()
        xml = self.__CleanUpXml(xml)

        if len(xml) == 0:
            return None

        try:
            it = ET.iterparse(StringIO(xml))
            for _, el in it:
                if '}' in el.tag:
                    el.tag = el.tag.split('}', 1)[1]  # strip all namespaces

            tree = it.root
            return tree
        except:
            dump = codecs.open(self.logpath + '/' + self.feedTitle + '.xml', 'w', 'utf-8-sig')
            dump.write(xml)
            dump.close()
            logging.exception(self.feedTitle + '('+self.feedLink+')')
            return None

    def __CleanUpHTML__(self, html):
        html = re.sub(r'(height|width)=\"\d+\"', '', html)
        html = re.sub(r'\.\./\.\.', self.feedDomain, html)
        html = html.strip()
        return html

    def __CleanUpXml(self, xml):
        charsToRemove = ['16'];
        for c in charsToRemove:
            xml = xml.replace(c.decode('hex'), '')

        return xml.strip()

    def __ParseRSS2__(self, xml):
        nodes = xml.findall('channel/item')
        date = datetime.now()

        for node in nodes:
            if node.find('updated') is not None:
                date = self.ParseDate(node.find('updated').text)

            if node.find('pubDate') is not None:
                date = self.ParseDate(node.find('pubDate').text)

            if date >= self.feedUpdated:
                n = {
                        'title': ''.join(node.find('title').itertext()).encode('utf-8'),
                        'link': node.find('link').text,
                        'id': str(uuid.uuid4()),
                        'feedId': self.feedId,
                        'description': ''
                    }

                n['updated'] = date

                if node.find('encoded') is not None:
                    n['description'] += self.__CleanUpHTML__(node.find('encoded').text.encode('utf-8'))
                elif node.find('description') is not None:
                    n['description'] += self.__CleanUpHTML__(''.join(node.find('description').itertext()).encode('utf-8'))

                enclosures = node.findall('enclosure');
                for enclosure in enclosures:
                    mimetype = enclosure.attrib['type']
                    src = enclosure.attrib['url']
                    if mimetype == 'image/jpeg':
                        n['description'] += '<p><img src="'+src+'"/></p>'
                    elif mimetype == 'video/mp4':
                        n['description'] += '<p><video controls="controls"><source src="'+src+'" type="'+mimetype+'"/></video></p>'
                    elif mimetype == 'video/mov':
                        n['description'] += '<p><video controls="controls"><source src="'+src+'" type="'+mimetype+'"/></video></p>'
                    else:
                        n['description'] += '<p><b>Enclosure not handled <a href="'+src+'">'+src+'</a> '+mimetype+'</b></p>'

                self.entries.append(n)

    def __ParseAtom1__(self, xml):
        nodes = xml.findall('entry')

        date = datetime.now()
        for node in nodes:
            date = self.ParseDate(node.find('updated').text)

            if date >= self.feedUpdated:
                n = {
                        'title': node.find('title').text.encode('utf-8'),
                        'link': node.find('link').attrib['href'],
                        'id': str(uuid.uuid4()),
                        'feedId': self.feedId,
                        'description': ''
                    }

                n['updated'] = date

                if node.find('content') is not None:
                    n['description'] = self.__CleanUpHTML__(node.find('content').text.encode('utf-8'))
                elif node.find('summary') is not None:
                    n['description'] = self.__CleanUpHTML__(node.find('summary').text.encode('utf-8'))

                self.entries.append(n)

    def ParseDate(self, strDate):
        #Date parsing in python2 is not that elegant and my handling is pretty crude to say the least.
        date = strDate
        date = date.replace('GMT','')
        date = date.replace('PST','')
        date = date.replace('UTC','')
        date = date.replace('EST','')
        date = date.replace('EDT','')
        date = date.replace('Z','')
        date = date.replace('z','')
        date = self.ConvertDayName(date) #Convert DANISH day name to english
        date = self.ConvertMonthName(date) #Convert DANISH month names into english

        format = ''
        newdate = None
        success = 0

        try:
            newdate = re.sub(r"([+-]([0-9)+:([0-9])+)$", '', date) #Remove offset in format [+-]00:00
            format = '%Y-%m-%dT%H:%M:%S'
            newdate = newdate.strip()
            newdate = datetime.strptime(newdate, format)
            success = 1
        except:
            success = 0

        if success == 0:
            try:
                newdate = re.sub(r"[+-]([0-9])+", "", date) #Remove offset in format [+-]0000
                format = '%a, %d %b %Y %H:%M:%S'
                newdate = newdate.strip()
                newdate = datetime.strptime(newdate, format)
                success = 1
            except:
                success = 0

        if success == 0:
            try:
                format = '%b %d, %Y'
                newdate = date
                newdate = newdate.strip()
                newdate = datetime.strptime(newdate, format)
                success = 1
            except:
                success = 0

        if success == 0:
            newdate = datetime.now()

        return newdate

    def ConvertDayName(self, name):
        name = name.replace('man', 'Mon')
        name = name.replace('tir', 'Tue')
        name = name.replace('ons', 'Wed')
        name = name.replace('tor', 'Thu')
        name = name.replace('fre', 'Fri')
        # name = name.replace('lør', 'Sat')
        # name = name.replace('søn', 'Sun')

        return name

    def ConvertMonthName(self, month):
        month = month.replace('maj', 'May')
        month = month.replace('okt', 'Oct')

        return month
