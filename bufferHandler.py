#!/opt/local/bin/python
# -*- coding: utf-8 -*-

import sqlite3 as lite
import sys
import logging
from feedLoader import *
from configLoader import *

class BufferHandler():

    con = None

    def __init__(self):
        self.config = ConfigLoader()
        self.logpath = self.config.Get('Path', 'LogPath')
        self.dbpath = self.config.Get('Path', 'DBPath')

        self.con = lite.connect(self.dbpath + '/data.sqlite')
        logging.basicConfig(level=logging.DEBUG, filename=self.logpath + '/dblog.txt')

    def __Open__(self):
        self.con = lite.connect(self.dbpath + '/data.sqlite')

    def AddEntry(self, entry):
        try:
            self.__Open__()
            cur = self.con.cursor()
            cur.execute('INSERT INTO entry(id, title, link, description, feedid, updated) VALUES(?, ?, ?, ?, ?, ?)', (entry['id'], entry['title'].decode('utf-8'), entry['link'], entry['description'].decode('utf-8'), entry['feedId'], entry['updated']))
            self.con.commit()
        except lite.Error, e:
            # logging.exception('AddEntry')
            # print e
            pass
        finally:
            if self.con:
                self.con.close()

    def GetAllInstantEntries(self):
        sql = 'SELECT feed.title AS feedTitle, feed.tags AS tags, entry.* FROM feed JOIN entry ON entry.feedid = feed.id WHERE feed.sendType = 1 AND entry.sent = 0 ORDER BY updated ASC'
        try:
            self.__Open__()
            self.con.row_factory = lite.Row
            cur = self.con.cursor()
            cur.execute(sql)

            data = cur.fetchall()

            return data
        except lite.Error, e:
            logging.exception('GetAllInstantEntries')
        finally:
            if self.con:
                self.con.close()

    def GetAllHourlyEntries(self, hour):
        sql = "SELECT feed.title AS feedTitle, feed.tags AS tags, entry.* FROM feed JOIN entry ON entry.feedid = feed.id WHERE feed.sendType = 2 AND feed.hourOfDay LIKE '%,"+`hour`+",%' AND entry.sent = 0 ORDER BY feed.title ASC"
        try:
            self.__Open__()
            self.con.row_factory = lite.Row
            cur = self.con.cursor()
            cur.execute(sql)

            data = cur.fetchall()

            return data
        except lite.Error, e:
            logging.exception('GetAllDailyEntries')
        finally:
            if self.con:
                self.con.close()


    def MarkSentEntry(self, id):
        success = 0

        try:
            self.__Open__()
            cur = self.con.cursor()
            cur.execute("UPDATE entry SET sent = 1 WHERE id = ?", [id])
            self.con.commit()

            success = 1
        except lite.Error, e:
            logging.exception('MarkSentEntry')
            success = 0
        finally:
            if self.con:
                self.con.close()

        return success

    def MarkSentEntries(self, ids):
        success = 0

        try:
            self.__Open__()
            idsLen = len(ids)

            for i in range(0, idsLen):
                id = ids[i]
                cur = self.con.cursor()
                cur.execute("UPDATE entry SET sent = 1 WHERE id = ?", [id])
                self.con.commit()

            success = 1
        except lite.Error, e:
            logging.exception('MarkSentEntry')
            success = 0
        finally:
            if self.con:
                self.con.close()

        return success

    def TrimBuffer(self):
        fl = FeedLoader()
        feeds = fl.GetAllFeeds()

        for feed in feeds:
            id = feed['id'];
            cleanbuffer = feed['cleanbuffer']

            if cleanbuffer == 1:
                try:
                    self.__Open__()
                    cur = self.con.cursor()
                    cur.execute("DELETE FROM [entry] WHERE updated < date('now', '-2 day') AND feedid = ?", [id])
                    self.con.commit()
                except:
                    logging.exception('TrimBuffer')
                finally:
                    if self.con:
                        self.con.close()
