#!/opt/local/bin/python
# -*- coding: utf-8 -*-

import sqlite3 as lite
import sys
import logging
from configLoader import *

class FeedLoader:

    con = None
    config = None

    def __init__(self):
        self.config = ConfigLoader()
        self.logpath = self.config.Get('Path', 'LogPath')
        self.dbpath = self.config.Get('Path', 'DBPath')

        self.con = lite.connect(self.dbpath + '/data.sqlite')
        logging.basicConfig(level=logging.DEBUG, filename=self.logpath + '/log.txt')

    def __Open__(self):
        self.con = lite.connect(self.dbpath + '/data.sqlite')

    def GetAllFeeds(self):
        sql = 'SELECT * FROM feed WHERE sendType > 0'

        try:
            self.__Open__()
            self.con.row_factory = lite.Row
            cur = self.con.cursor()
            cur.execute(sql)

            data = cur.fetchall()

            return data
        except lite.Error, e:
            print "Error %s: " % e.args[0]
        finally:
            if self.con:
                self.con.close()

    def SetLastUpdated(self, id):
        sql = "UPDATE [feed] SET updated = datetime('now') WHERE id = ?"
        try:
            self.__Open__()
            cur = self.con.cursor()
            cur.execute(sql, [id])
            self.con.commit()

        except:
            logging.exception('SetLastUpdated')
        finally:
            if self.con:
                self.con.close()


