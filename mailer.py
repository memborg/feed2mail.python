#!/opt/local/bin/python
# -*- coding: utf-8 -*-

from bufferHandler import *
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.Header import Header
from email.Utils import parseaddr, formataddr
import logging
import codecs
from configLoader import *
from datetime import datetime

class Mailer():

    def __init__(self):
        self.config = ConfigLoader()
        self.logpath = self.config.Get('Path', 'LogPath')
        self.templatepath = self.config.GetAppDir()
        self.fromEmail = self.config.Get('Mail','From')
        self.toEmail = self.config.Get('Mail', 'To')

    def SendInstant(self):

        bh = BufferHandler()
        entries = bh.GetAllInstantEntries()

        template = codecs.open(self.templatepath + '/Instant.html', 'r', 'utf-8').read()

        for entry in entries:
            body = template.replace('{title}', entry['title'])
            body = body.replace('{description}', entry['description'])
            body = body.replace('{link}', entry['link'])
            body = body.replace('{feedtitle}', entry['feedTitle'])
            body = body.replace('{tags}', entry['tags'])
            try:
                dt = datetime.strptime(entry['updated'], '%Y-%m-%d %H:%M:%S')
                body = body.replace('{updated}', '{0.day}-{0.month}-{0.year} {0.hour}:{0.minute}'.format(dt))
            except:
                body = body.replace('{updated}', '')



            subject = entry['title'] + ' ' + entry['tags']

            success = self.Send(entry['feedTitle'] + ' <'+self.fromEmail+'>', subject, body)

            if success == 1:
                bh.MarkSentEntry(entry['id'])

    def SendHourly(self):
        date = datetime.now()
        hour = date.hour
        bh = BufferHandler()
        entries = bh.GetAllHourlyEntries(hour)

        index = ''
        entriesText = ''
        template = codecs.open(self.templatepath + '/hourly.html', 'r', 'utf-8').read()
        body = ''
        handled = []
        ids = []
        current = ''
        feedTitle = ''
        tags = ''
        for entry in entries:
            feedId = entry['feedId']
            if current != feedId:
                if(index != ''):
                    body = template.replace('{title}', feedTitle)
                    body = body.replace('{index}', index)
                    body = body.replace('{entries}', entriesText)
                    subject = feedTitle + ' ' + tags

                    success = self.Send(feedTitle + ' <'+self.fromEmail+'>', subject, body)

                    if success == 1:
                        bh.MarkSentEntries(ids)

                ids = []
                index = ''
                entriesText = ''
                current = feedId
                feedTitle = entry['feedtitle']
                tags = entry['tags']


            ids.append(entry['id'])
            index += '<li><a href="#'+entry['id']+'">'+entry['title']+'</a></li>'
            entriesText += '<div>'
            entriesText += '<a name="'+entry['id']+'"></a>'
            entriesText += '<h2>'+entry['title']+'</h2>'
            try:
                dt = datetime.strptime(entry['updated'], '%Y-%m-%d %H:%M:%S')
                entriesText += '<span class="updated">Dato: '+'{0.day}-{0.month}-{0.year} {0.hour}:{0.minute}'.format(dt)+'</span>'
            except:
                pass

            entriesText += '<p>'+entry['description']+'</p>'
            entriesText += '<p><a href="'+entry['link']+'" style="color: bluesteel; text-decoration: none;">Read more&hellip;</a></p>'
            entriesText += '</div>'
            entriesText += '<hr/>'


    def Send(self, sender, subject, body):
        logging.basicConfig(level=logging.DEBUG, filename=self.logpath + '/maillog.txt')
        success = 0

        try:
            # Header class is smart enough to try US-ASCII, then the charset we
            # provide, then fall back to UTF-8.
            header_charset = 'ISO-8859-1'

            # We must choose the body charset manually
            for body_charset in 'US-ASCII', 'ISO-8859-1', 'UTF-8':
                try:
                    body.encode(body_charset)
                except UnicodeError:
                    pass
                else:
                    break

            # Split real name (which is optional) and email address parts
            sender_name, sender_addr = parseaddr(sender)

            recipient_name, recipient_addr = parseaddr(self.toEmail)

            # We must always pass Unicode strings to Header, otherwise it will
            # use RFC 2047 encoding even on plain ASCII strings.
            sender_name = str(Header(unicode(sender_name), header_charset))
            recipient_name = str(Header(unicode(recipient_name), header_charset))

            # Make sure email addresses do not contain non-ASCII characters
            sender_addr = sender_addr.encode('ascii')
            recipient_addr = recipient_addr.encode('ascii')

            msg = MIMEMultipart('alternative')
            msg['From'] = formataddr((sender_name, sender_addr))
            msg['To'] = formataddr((recipient_name, recipient_addr))
            msg['Subject'] = Header(unicode(subject), header_charset)

            part = MIMEText(body.encode(body_charset), 'html', body_charset)

            msg.attach(part)

            s = smtplib.SMTP(self.config.Get('Mail', 'SmtpHost')+':'+ self.config.Get('Mail', 'SmtpPort'))
            s.starttls()
            s.login(self.config.Get('Mail', 'SmtpUsername'), self.config.Get('Mail', 'SmtpPassword'))
            s.sendmail(self.fromEmail, self.toEmail, msg.as_string())
            s.quit()
            success = 1
        except:
            logging.exception(subject)
            success = 0

        return success
