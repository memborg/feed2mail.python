#!/opt/local/bin/python
# -*- coding: utf-8 -*-
from feedLoader import *
from feedChecker import *
from bufferHandler import *
from mailer import *

if __name__ == '__main__':
    fl = FeedLoader()
    feeds = fl.GetAllFeeds()

    for feed in feeds:
        Checker(feed)
        fl.SetLastUpdated(feed['id'])

    bh = BufferHandler()
    bh.TrimBuffer()

    mailer = Mailer()
    mailer.SendInstant()
    mailer.SendHourly()
